package errortracking

// The configuration for Tracker.
type trackerConfig struct {
	sentryDSN         string
	version           string
	sentryEnvironment string
	loggerName        string
}

// TrackerOption will configure a Tracker.
type TrackerOption func(*trackerConfig)

func applyTrackerOptions(opts []TrackerOption) trackerConfig {
	config := trackerConfig{}

	for _, v := range opts {
		v(&config)
	}

	return config
}

// WithSentryDSN sets the sentry data source name.
func WithSentryDSN(sentryDSN string) TrackerOption {
	return func(config *trackerConfig) {
		config.sentryDSN = sentryDSN
	}
}

// WithVersion is used to configure the version of the service
// that is currently running.
func WithVersion(version string) TrackerOption {
	return func(config *trackerConfig) {
		config.version = version
	}
}

// WithSentryEnvironment sets the sentry environment.
func WithSentryEnvironment(sentryEnvironment string) TrackerOption {
	return func(config *trackerConfig) {
		config.sentryEnvironment = sentryEnvironment
	}
}

// WithLoggerName sets the logger name.
func WithLoggerName(loggerName string) TrackerOption {
	return func(config *trackerConfig) {
		config.loggerName = loggerName
	}
}
