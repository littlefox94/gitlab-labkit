package errortracking

// Capture reports an error to the error reporting service.
func Capture(err error, opts ...CaptureOption) {
	DefaultTracker().Capture(err, opts...)
}
