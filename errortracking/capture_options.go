package errortracking

import (
	"github.com/getsentry/sentry-go"
)

// CaptureOption will configure how an error is captured.
type CaptureOption func(*captureConfig, *sentry.Event)

type captureConfig struct{}

func applyCaptureOptions(opts []CaptureOption) (captureConfig, *sentry.Event) {
	event := sentry.NewEvent()
	event.Level = sentry.LevelError

	config := captureConfig{}

	for _, v := range opts {
		v(&config, event)
	}

	return config, event
}
